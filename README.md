# Tetris
### A simple tetris game, with a javaFx GUI
### by Dorian Verriere & Thibault Delorme

---

To play, press:

* left and right to move the tetrimino
* bottom to make it fall
* up or space to spin it

**Implemented ** :

* create a board with dimensions 10 by 24
* create random Tetrimos
```
x
x           x      x        x    x        x
x    x x    x x    x x    x x    x        x
x    x x    x        x    x      x x    x x
```
* every seconds the Tetrimino fall one cell down
* when the tetrimino can't move anymore, it is blocked and an other appear
* each line completed is deleted, then the rows above move down
* it's possible to rotate tetriminos
* it's possible to move tetriminos on right and left
* when a new tetrimino can't be placed, the game is over and stop

**Bonus:**

* score: a single line cleared is 100 points, a tetris (four lines cleared simultaneously) is worth 800. A back-to-back Tetris is worth 1200, and a back-ta-back line give 200.
* GUI created with javaFx
* press bottom make the piece falling quick

---

Developped by Dorian and Thibault, APP4 Info Polytech Paris Sud
