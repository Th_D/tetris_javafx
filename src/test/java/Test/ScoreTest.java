package Test;

import Game.Score;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class ScoreTest {

    /* TEST CREATION SCORE */
    @Test
    public void should_be_supp_or_equal_to_zero() {
        Score score = new Score();
        assertThat(score.getScore()).isGreaterThan(-1);
    }

    /* TEST METHODES ADD SCORE */
    @Test
    public void should_be_add_100_to_score_singleLine() {
        Score score = new Score();
        score.singleLine();
        assertThat(score.getScore()).isEqualTo(100);
    }
    @Test
    public void should_be_add_800_to_score_tetris() {
        Score score = new Score();
        score.tetris();
        assertThat(score.getScore()).isEqualTo(800);
    }
    @Test
    public void should_be_add_1200_to_score_backToBackTetris() {
        Score score = new Score();
        score.backToBackTetris();
        assertThat(score.getScore()).isEqualTo(1200);
    }
    @Test
    public void should_be_add_200_to_score_backToBack() {
        Score score = new Score();
        score.backToBack();
        assertThat(score.getScore()).isEqualTo(200);
    }


    /* TEST With UPDATE */
    @Test
    public void should_be_200_to_score() {
        Score score = new Score();
        score.update(1);
        assertThat(score.getScore()).isEqualTo(200);
    }

    @Test
    public void should_be_add_1200_to_score() {
        Score score = new Score();
        score.update(4);
        assertThat(score.getScore()).isEqualTo(1200);
    }



}
