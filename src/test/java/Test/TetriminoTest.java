package Test;

import Game.State;
import Game.Tetrimino;
import org.junit.Test;

import java.util.Random;
import static junit.framework.Assert.assertNotNull;
import static org.assertj.core.api.Assertions.assertThat;

public class TetriminoTest {

    private static final int TETRIMINO_BORN_X = 0;
    private static final int TETRIMINO_BORN_Y = 4;

    private static final int INIT_TETRIMINO_BORN_X = 0;
    private static final int INIT_TETRIMINO_BORN_Y = 0;

    @Test
    public void Create_Random_Tetrimino_return_not_null() throws InterruptedException {
        Random random = new Random();
        Tetrimino TetriNotNull = new Tetrimino(State.values()[random.nextInt(State.values().length)], TETRIMINO_BORN_X, TETRIMINO_BORN_Y);
        assertNotNull(TetriNotNull);
    }

    /* Tester State*/
    @Test
    public void should_be_equal_to_State()throws InterruptedException  {
        Tetrimino TetriStateI = new Tetrimino(State.T, TETRIMINO_BORN_X, TETRIMINO_BORN_Y);
        assertThat(TetriStateI.getType()).isEqualTo(State.T); // T
    }

    /* Tester couleur*/
    @Test
    public void should_be_equal_to_State_who_represent_the_color()throws InterruptedException  {
        Tetrimino TetriStateI= new Tetrimino(State.I, TETRIMINO_BORN_X, TETRIMINO_BORN_Y);
        assertThat(TetriStateI.getColor()).isEqualTo(Tetrimino.colorTetriminos.get(State.I)); // aqua
    }

    /* Tester position Initial*/
    @Test
    public void should_be_equal_to_the_position_initial()throws InterruptedException  {
        Tetrimino TetriStateI= new Tetrimino(State.I, INIT_TETRIMINO_BORN_X, INIT_TETRIMINO_BORN_Y);
        assertThat(TetriStateI.getPoints()).isEqualTo(Tetrimino.shapeTetriminos.get(State.I));
    }

    /* Tester mouvement gauche */
    @Test
    public void should_be_move_on_the_Left()throws InterruptedException  {
        Tetrimino TetriStateI= new Tetrimino(State.I, TETRIMINO_BORN_X, TETRIMINO_BORN_Y);
        TetriStateI.moveLeft();
        for (int i = 0; i < 4; i++){
            assertThat(TetriStateI.getPoints().get(0).x).isEqualTo(Tetrimino.shapeTetriminos.get(State.I).get(0).x + TETRIMINO_BORN_X);
            assertThat(TetriStateI.getPoints().get(0).y).isEqualTo(Tetrimino.shapeTetriminos.get(State.I).get(0).y + TETRIMINO_BORN_Y - 1);
        }
    }

    /* Tester mouvement droite */
    @Test
    public void should_be_move_on_the_right()throws InterruptedException  {
        Tetrimino TetriStateI= new Tetrimino(State.I, TETRIMINO_BORN_X, TETRIMINO_BORN_Y);
        TetriStateI.moveRight();
        for (int i = 0; i < 4; i++){
            assertThat(TetriStateI.getPoints().get(0).x).isEqualTo(Tetrimino.shapeTetriminos.get(State.I).get(0).x + TETRIMINO_BORN_X);
            assertThat(TetriStateI.getPoints().get(0).y).isEqualTo(Tetrimino.shapeTetriminos.get(State.I).get(0).y + TETRIMINO_BORN_Y + 1);
        }
    }

    /* Tester methode fall */
    @Test
    public void should_be_move_on_the_bottom()throws InterruptedException  {
        Tetrimino TetriStateI= new Tetrimino(State.I, TETRIMINO_BORN_X, TETRIMINO_BORN_Y);
        TetriStateI.fall();
        for (int i = 0; i < 4; i++){
            assertThat(TetriStateI.getPoints().get(i).x).isEqualTo(Tetrimino.shapeTetriminos.get(State.I).get(i).x + TETRIMINO_BORN_X + 1);
            assertThat(TetriStateI.getPoints().get(i).y).isEqualTo(Tetrimino.shapeTetriminos.get(State.I).get(i).y + TETRIMINO_BORN_Y );
        }
    }



    /* Tester rotation pour chacun des States de pièces ( */
    @Test
    public void should_be_revert_on_itself()throws InterruptedException  {
        // TODO
        Tetrimino TetriRevert= new Tetrimino(State.I, TETRIMINO_BORN_X, TETRIMINO_BORN_Y);
        Tetrimino TetriCopie = TetriRevert;
        TetriRevert.spin();
        assertThat(TetriRevert.getPoints()).isEqualTo(TetriCopie.getPoints());

    }


}
