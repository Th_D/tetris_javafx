package Test;

import Game.Board;
import Game.State;
import org.junit.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class BoardTest {


    @Test
    public void Size_L_Should_Be_10() {
        Board board = new Board();
        assertThat(board.getSize_L()).isEqualTo(10);
    }

    @Test
    public void Size_H_Should_Be_24() {
        Board board = new Board();
        assertThat(board.getSize_H()).isEqualTo(24);
    }

    /* Tester si à l'initialisation, la grid est vide => sans tetrimino */
    @Test
    public void BoardInitialIsEmpty() {
        Board board = new Board();
        for (int x = 0; x < board.getSize_H(); x++) {
            for (int y = 0; y < board.getSize_L(); y++) {
                assertThat(board.getCell(x, y)).isEqualTo(State.empty);
            }
        }
    }

    @Test
    public void should_be_equal_to_State_Z() {
        Board board = new Board();
        board.setCell(5,8,State.Z);
        assertThat(board.getCell(5, 8)).isEqualTo(State.Z);
    }

    @Test
    public void should_be_not_full() {
        Board board = new Board();
        board.setCell(5,8,State.Z);
        assertThat(board.checkFullLine2(5)).isFalse();
    }

    @Test
    public void should_be_full() {
        Board board = new Board();
        board.setCell(5,0,State.Z);
        board.setCell(5,1,State.Z);
        board.setCell(5,2,State.Z);
        board.setCell(5,3,State.Z);
        board.setCell(5,4,State.Z);
        board.setCell(5,5,State.Z);
        board.setCell(5,6,State.Z);
        board.setCell(5,7,State.Z);
        board.setCell(5,8,State.Z);
        board.setCell(5,9,State.Z);
        assertThat(board.checkFullLine2(5)).isTrue();
    }
}
