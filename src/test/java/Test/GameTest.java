package Test;

import Game.Board;
import Game.Tetris;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class GameTest {

    @Test
    public void should_be_in_progress() throws InterruptedException {
        Board board = new Board();
        Tetris tetris = new Tetris(board);
        assertThat(tetris.onGoingGame()).isFalse();
    }
}

