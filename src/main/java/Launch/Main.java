package Launch;


import GUI.Gui;
import Game.Board;
import Game.Tetris;
import javafx.application.Application;

public class Main {
    public static void main(String[] args) throws InterruptedException {
        Board board = new Board();
        Gui.setBoard(board);
        new Thread(new Runnable() {
            public void run() {
                Application.launch(Gui.class);
            }
        }).start();
        Tetris tetris = new Tetris(board);
    }
}
