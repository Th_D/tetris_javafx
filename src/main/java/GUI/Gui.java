package GUI;

import Game.Board;
import Game.Tetrimino;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

import java.awt.*;
import java.util.ArrayList;
import java.util.Random;

public class Gui extends Application {

    private static final int    GRID_SIZE_GAP   =  3;
    private static final int    CELL_SIZE       =  20;
    private static final String STYLE_SCORE     =  "-fx-text-fill: WHITE;" +
                                                   "-fx-font-size: 26 px;" +
                                                   "-fx-text-alignment: center";
    private static final String SCORE_LABEL_TEXT = "SCORE : ";


    private static boolean ready = false;
    private static Scene scene;
    private static ArrayList<ArrayList<Label>> grid = new ArrayList();
    private static Board board;
    private static Label labelScore;
    private static int currentScore;


    //ACCESS

    public static void setBoard(Board b) { board = b; }

    public static boolean isReady() { return ready; }

    public static Scene getScene() { return scene; }


    //APPLICATION OVERRIDE
    @Override
    public void start(Stage primaryStage) {
        GridPane root = new GridPane();
        scene = new Scene(root);
        root.setHgap(GRID_SIZE_GAP);
        root.setVgap(GRID_SIZE_GAP);
        root.setAlignment(Pos.CENTER);

        for (int i = 0; i < board.getSize_H(); i++) {
            grid.add(new ArrayList());
            for (int j = 0; j < board.getSize_L(); j++) {
                Label label = new Label();

                label.setStyle("-fx-background-color : grey");
                label.setPrefSize(CELL_SIZE, CELL_SIZE);

                grid.get(grid.size() - 1).add(label);
                root.add(label, j, i);
            }
        }

        // Set the border-color of the GridPane
        root.setStyle("-fx-background-color : black");
        // Set the size of the GridPane
        root.setPrefSize(board.getSize_L() * CELL_SIZE + GRID_SIZE_GAP * board.getSize_L(),
                board.getSize_H()* CELL_SIZE + GRID_SIZE_GAP * board.getSize_H()+30);

        // Score label
        labelScore = new Label(SCORE_LABEL_TEXT);
        labelScore.setStyle(STYLE_SCORE);
        root.add(labelScore, 0,  board.getSize_H(), board.getSize_L(), 1);
        // Add the scene to the Stage
        primaryStage.setScene(scene);
        // Set the title of the Stage
        primaryStage.setTitle("Tetris");
        // Display the Stage
        primaryStage.show();

        ready = true;
    }


    //PUBLIC DISPLAY METHODS

    public static void loose() {
        Random r = new Random();
        for (int i = 0; i < board.getSize_H(); i++) {
            for (int j = 0; j < board.getSize_L(); j++) {
                Gui.colorCell(i, j, "rgba(" + r.nextInt(256)
                        + "," + r.nextInt(256)
                        + "," + r.nextInt(256)
                        + ",1)");
            }
        }
    }

    public static void updateAll(Board board){
       // System.out.println("UPDATE ALL");
        for(int i = board.getSize_H()-1; i>1 ; i--)
            for(int j = 0; j< board.getSize_L(); j++)
                colorCell(i, j, Tetrimino.getColor(board.getCell(i,j)));
        Platform.runLater(()->labelScore.setText(SCORE_LABEL_TEXT + currentScore));
    }

    public static void updateScore(int score) {
        currentScore = score;
    }

    public static void colorCell(Point p, String color) {
        colorCell(p.x, p.y, color);
    }

    //PRIVATE METHODS

    private static void colorCell(int x, int y, String color) {
        if(board != null) // usefull when TetriminoTest use a null board
            if ((y >= 0) && (y < board.getSize_L()) && (x >= 0) && (x < board.getSize_H()))
                grid.get(x).get(y).setStyle("-fx-background-color : " + color);

    }

}

