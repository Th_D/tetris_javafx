package Game;

import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;

public class Controller {

    // CONST : control keys
    private static final KeyCode left    = KeyCode.LEFT;
    private static final KeyCode right   = KeyCode.RIGHT;
    private static final KeyCode down    = KeyCode.DOWN;
    private static final KeyCode space   = KeyCode.SPACE;
    private static final KeyCode up      = KeyCode.UP;

    //CONSTRUCTOR
    //CREATE HANDLER to get keyboard input and associate theme with game commands
    Controller(Scene scene, final Tetris tetris) {
        scene.addEventHandler(KeyEvent.KEY_PRESSED, new EventHandler<KeyEvent>() {
            public void handle(KeyEvent key) {
                if (tetris.onGoingGame()) {
                    if (key.getCode() == right) {
                        try {
                            tetris.moveRight();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                    if (key.getCode() == left) {
                        try {
                            tetris.moveLeft();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                    if (key.getCode() == down) {
                        try {
                            tetris.fall();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                    if ((key.getCode() == space) || (key.getCode() == up)) {
                        try {
                            tetris.revert();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        });
    }
}

