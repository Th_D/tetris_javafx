package Game;

public class Score {
    //CONST
    private static final int SINGLE_LINE_SCORE          = 100;
    private static final int TETRIS_SCORE               = 800;
    private static final int BACK_TO_BACK_TETRIS_SCORE  = 1200;
    private static final int BACK_TO_BACK_SCORE         = 200;

    //ATTRIBUTE
    private int score;
    private int lastNbCompleteLine;


    //ACCESS
    public int getScore(){ return score; }

    //METHODS
    public void singleLine()      { score += SINGLE_LINE_SCORE  ; }
    public void tetris()          { score += TETRIS_SCORE       ; }
    public void backToBack()       { score += BACK_TO_BACK_SCORE ; }
    public void backToBackTetris() { score += BACK_TO_BACK_TETRIS_SCORE ; }


    public Score(){
        score = 0;
        lastNbCompleteLine = 0;
    }


    public void update(int nbCompleteLine) {
        lastNbCompleteLine = nbCompleteLine;

        if (lastNbCompleteLine != 0) {
            if (nbCompleteLine == 4)
                backToBackTetris();
            else
                backToBack();
        } else {
            if (nbCompleteLine < 4)
                for (int i = 0; i < nbCompleteLine; i++)
                    singleLine();

            if (nbCompleteLine == 4)
                tetris();
        }
    }
}
