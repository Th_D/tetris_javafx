package Game;

import GUI.Gui;

import java.awt.*;
import java.util.ArrayList;
import java.util.HashMap;

public class Tetrimino {

    //CONST
    static final int TETRIMINO_BORN_X = 0;
    static final int TETRIMINO_BORN_Y = 4;

    //STATIC
    public static HashMap<State, ArrayList<Point>> shapeTetriminos;
    public static HashMap<State, String> colorTetriminos;
    private static HashMap<State, Integer> nbRotationTetriminos;

    //ATTRIBUTES
    State type;
    private ArrayList<Point> blocs;
    private int nbRotation;
    private int x;
    private int y;
    private boolean statusRatation;

    public Tetrimino(State type, int x, int y) throws InterruptedException {
        this.x = x;
        this.y = y;
        this.nbRotation = nbRotationTetriminos.get(type);
        statusRatation = false;
        this.type = type;
        this.blocs = new ArrayList();
        for (Point p : shapeTetriminos.get(type)) {
            this.blocs.add(new Point(p.x + this.x, p.y + this.y));
        }
    }

    Tetrimino(State type) throws InterruptedException {
        this(type, TETRIMINO_BORN_X, TETRIMINO_BORN_Y);
    }

    public ArrayList<Point> getPoints() {
        return blocs;
    }

    public String getColor() {return colorTetriminos.get(type);}
    public static String getColor(State s) {return colorTetriminos.get(s);}


    private void clear() {
        for (Point p : this.blocs) {
            //System.out.println("CLEAR x=" + p.x + "\ty=" + p.y);
            Gui.colorCell(p, "grey");
        }
    }

    public void fall() {
        clear();
        this.x++;
        for (Point p : blocs) {
            p.x++;
        }
    }

    public void moveLeft() {
        clear();
        this.y--;
        for (Point p : blocs) {
            p.y--;
        }
    }

    public void moveRight() {
        clear();
        this.y++;
        for (Point p : blocs) {
            p.y++;
        }
    }

    public void spin() {
        System.out.println("Tetromino.revert()");
        this.clear();
        if(this.nbRotation == 4) {
            this.rotate90();
        }
        else if (nbRotation == 2){
            if(this.statusRatation) {
                this.rotate90();
            }
            else {
                for(int i = 0; i<3; i++){
                    this.rotate90();
                }
            }
        }
        this.draw();
    }

    private void rotate90() {
        // x1 = -y = yPivot - yBrickCenter
        // y1 = x = xBrickCenter - xPivot
        for (Point p : this.blocs) {
            int temp = this.y - this.x + p.x;
            p.x = this.x + this.y - p.y;
            p.y = temp;
        }
    }

    public void antiSpin(){
        this.clear();
        System.out.println("Tetromino.antiRevert()");
        for (int i  = 0; i<3; i++) {
            this.spin();
        }
        this.draw();
    }

    void draw() {
        for (Point p : this.blocs) {
            Gui.colorCell(p, this.getColor());
        }
    }

    //TEST
    public State getType() {
        for(State key: colorTetriminos.keySet()) {
            if(colorTetriminos.get(key).equals(this.getColor())) {
                return key;
            }
        }
        return null;
    }

    static {
        //System.out.println("INIT");
        colorTetriminos = new HashMap<>();
        colorTetriminos.put(State.I, "aqua");
        colorTetriminos.put(State.O, "yellow");
        colorTetriminos.put(State.T, "purple");
        colorTetriminos.put(State.L, "orange");
        colorTetriminos.put(State.J, "blue");
        colorTetriminos.put(State.Z, "red");
        colorTetriminos.put(State.S, "green");
        colorTetriminos.put(State.empty, "grey");

        nbRotationTetriminos = new HashMap<>();
        nbRotationTetriminos.put(State.I, 2);
        nbRotationTetriminos.put(State.O, 1);
        nbRotationTetriminos.put(State.T, 4);
        nbRotationTetriminos.put(State.L, 4);
        nbRotationTetriminos.put(State.J, 4);
        nbRotationTetriminos.put(State.Z, 2);
        nbRotationTetriminos.put(State.S, 2);


        shapeTetriminos = new HashMap<>();
        shapeTetriminos.put(State.I, new ArrayList());
        shapeTetriminos.get(State.I).add(new Point(0, -1));
        shapeTetriminos.get(State.I).add(new Point(0, 0));
        shapeTetriminos.get(State.I).add(new Point(0, 1));
        shapeTetriminos.get(State.I).add(new Point(0, 2));

        shapeTetriminos.put(State.O, new ArrayList());
        shapeTetriminos.get(State.O).add(new Point(0, 0));
        shapeTetriminos.get(State.O).add(new Point(0, 1));
        shapeTetriminos.get(State.O).add(new Point(1, 0));
        shapeTetriminos.get(State.O).add(new Point(1, 1));

        shapeTetriminos.put(State.T, new ArrayList());
        shapeTetriminos.get(State.T).add(new Point(0, -1));
        shapeTetriminos.get(State.T).add(new Point(0, 0));
        shapeTetriminos.get(State.T).add(new Point(0, 1));
        shapeTetriminos.get(State.T).add(new Point(1, 0));

        shapeTetriminos.put(State.L, new ArrayList());
        shapeTetriminos.get(State.L).add(new Point(0, -1));
        shapeTetriminos.get(State.L).add(new Point(0, 0));
        shapeTetriminos.get(State.L).add(new Point(0, 1));
        shapeTetriminos.get(State.L).add(new Point(1, -1));

        shapeTetriminos.put(State.J, new ArrayList());
        shapeTetriminos.get(State.J).add(new Point(0, -1));
        shapeTetriminos.get(State.J).add(new Point(0, 0));
        shapeTetriminos.get(State.J).add(new Point(0, 1));
        shapeTetriminos.get(State.J).add(new Point(1, 1));

        shapeTetriminos.put(State.Z, new ArrayList());
        shapeTetriminos.get(State.Z).add(new Point(0, -1));
        shapeTetriminos.get(State.Z).add(new Point(1, 0));
        shapeTetriminos.get(State.Z).add(new Point(0, 0));
        shapeTetriminos.get(State.Z).add(new Point(1, 1));

        shapeTetriminos.put(State.S, new ArrayList());
        shapeTetriminos.get(State.S).add(new Point(0, 1));
        shapeTetriminos.get(State.S).add(new Point(0, 0));
        shapeTetriminos.get(State.S).add(new Point(1, 0));
        shapeTetriminos.get(State.S).add(new Point(1, -1));
    }

}