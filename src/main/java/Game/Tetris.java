package Game;

import GUI.Gui;

import java.awt.*;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import static Game.Tetrimino.TETRIMINO_BORN_X;
import static Game.Tetrimino.TETRIMINO_BORN_Y;


public class Tetris {

    //ATTRIBUTE
    private boolean lost = false;
    private Tetrimino currentTetrimino;
    private Board board;
    private Score score;

    //CONSTRUCTOR
    public Tetris(Board board) throws InterruptedException {

        this.board = board;
        this.score = new Score();

        //wait for the GUI to be fully constructed
        while (!Gui.isReady()) {
            try {
                TimeUnit.MILLISECONDS.sleep(5);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        Controller controller = new Controller(Gui.getScene(), this);

        //create the first tetrimino
        createTetrimino();

        //game loop
        do {
            TimeUnit.SECONDS.sleep(1);
            this.fall();
            Gui.updateAll(board);
            Gui.updateScore(score.getScore());
            currentTetrimino.draw();
        } while (!lost);
    }


    //ACCESS

    public boolean onGoingGame(){ return !this.lost; }


    //PUBLIC METHODS

    void fall() throws InterruptedException {
        if (checkCollision(this.currentTetrimino, 1, 0)) {
            this.currentTetrimino.fall();
        }
        currentTetrimino.draw();
    }

    void moveRight() throws InterruptedException {
        if (checkCollision(this.currentTetrimino, 0, 1)) {
            this.currentTetrimino.moveRight();
        }
        currentTetrimino.draw();
        //loose();
    }

    void moveLeft() throws InterruptedException {
        if (checkCollision(this.currentTetrimino, 0, -1)) {
            this.currentTetrimino.moveLeft();
        }
        currentTetrimino.draw();
    }

    void revert() throws InterruptedException {
        this.currentTetrimino.spin();
        if(!checkCollision(currentTetrimino, 0, 0))
            this.currentTetrimino.antiSpin();
    }


    //PRIVATE METHODS

    private void checkLoose() {
        for (Point p : currentTetrimino.getPoints()) {
            if ((board.getCell(p.x, p.y)) != (State.empty)) {
                this.loose();
                break;
            }
        }
    }

    private void createTetrimino() throws InterruptedException {
        Random random = new Random();
        currentTetrimino = new Tetrimino(State.values()[random.nextInt(State.values().length-1)+1], TETRIMINO_BORN_X, TETRIMINO_BORN_Y);
        this.checkLoose();
        currentTetrimino.draw();
    }

    private boolean checkCollision(Tetrimino tetrimino, int x_movement, int y_movement) throws InterruptedException {
        for (Point p : tetrimino.getPoints()) {
            if ((p.y + y_movement < 0) || (p.y + y_movement >= board.getSize_H()) || ((p.y + y_movement) >= board.getSize_L()) || (p.x + x_movement < 0)) {
                return false;
            }
            if (((p.x + x_movement) == board.getSize_H())) {
                stickTetrimino(tetrimino);
                return false;
            }

            if (board.getCell((p.x+x_movement),(p.y+y_movement)) != State.empty) {
                if (x_movement == 1) {
                    stickTetrimino(tetrimino);
                }
                return false;
            }
        }
        return true;
    }

    void stickTetrimino(Tetrimino tetrimino) throws InterruptedException {
        for (Point p : tetrimino.getPoints()) {
            board.setCell(p.x, p.y, tetrimino.type);
            if(p.y == 0)
                System.out.println(p.x + " board.getCell(p.x, p.y) = " + board.getCell(p.x, p.y));

        }
        if(!lost) {
            createTetrimino();
            int nbCompleteLine = board.checkFullLine();
            score.update(nbCompleteLine);
        }
    }

    private void loose() {
        lost = true;
        Gui.loose();
    }

}
