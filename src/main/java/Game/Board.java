package Game;

import GUI.Gui;

import java.util.ArrayList;

public class Board {

    //CONST
    private static final int GRID_SIZE_L    = 10;
    private static final int GRID_SIZE_H    = 24;


    //ACCESS
    public int getSize_L(){ return GRID_SIZE_L; }
    public int getSize_H(){ return GRID_SIZE_H; }

    //ATTRIBUTE
    private ArrayList<ArrayList<State>> grid = new ArrayList();

    //CONSTRUCTOR
    public Board(){
        for (int i = 0; i < GRID_SIZE_H; i++) {
            grid.add(new ArrayList());
            for (int j = 0; j < GRID_SIZE_L; j++) {
                grid.get(i).add(State.empty);
            }
        }
    }

    //PUBLIC METHODS
    public State getCell(int x, int y){
        return grid.get(x).get(y);
    }

    public void setCell(int x, int y, State state){
        if(x>=0 && y>=0 && x<GRID_SIZE_H && y<GRID_SIZE_L)
            grid.get(x).set(y, state);
    }

    int checkFullLine(){
        int ret = 0;
        for(int i = 0; i<GRID_SIZE_H; i++) {
            if(checkFullLine(i)){
                ret++;
            }
        }
        return ret;
    }


    //PRIVATE METHODS
    private boolean checkFullLine(int line){
        if(checkFullLine(grid.get(line))){
            this.deleteLine(line);
            return true;
        }
        return false;
    }

    private boolean checkFullLine(ArrayList<State> line){
        for(State s : line)
            if (s == State.empty)
                return false;
        return true;
    }

    public boolean checkFullLine2(int line){
        if(checkFullLine(grid.get(line))){
        /*(int i = 0; i<GRID_SIZE_L; i++)
            Gui.colorCell(line, i, "yellow");*/
            return true;
        }
        return false;
    } //TODO : removec

    private void deleteLine(int line) {
        this.grid.remove(line);
        this.grid.add(0, new ArrayList());
        for (int j = 0; j < GRID_SIZE_L; j++) {
            grid.get(0).add(State.empty);
        }
        Gui.updateAll(this);
    }

}
